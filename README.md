#### 我的博客: [https://www.cnblogs.com/cloud--/](https://www.cnblogs.com/cloud--/)
# 周志华-机器学习-西瓜数据集

#### 介绍
　　周志华《机器学习》中的西瓜数据集, 方便下载完成习题.<br>
　　目前只会有习题中需要的．<br>
　　为了方便修改了一小部分, 比如编号都是从 0 开始 (程序员嘛...), 中文属性用英文代替 (防止plt乱码) 等等. 当然数据内容没有变动.